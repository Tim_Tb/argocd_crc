FROM nginxinc/nginx-unprivileged:latest

EXPOSE 8080

COPY index.html /usr/share/nginx/html/
COPY styles.css /usr/share/nginx/html/
